#ifndef PIXEL_H
#define PIXEL_H

struct pixel {
    unsigned char b, g, r;
};

#endif
