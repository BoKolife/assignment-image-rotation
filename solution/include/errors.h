#ifndef ERRORS_H
#define ERRORS_H  

#include <stdio.h>
#include <stdlib.h>

enum open_status{
    OPEN_OK = 0,
    FILE_NOT_FOUND
};

/* for read_from_bmp()   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_DATA,
    READ_INVALID_FSEEK,
    READ_INVALID_HEADER,
    READ_INVALID_FILE
};

/* for write_to_bmp()   */
enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_FSEEK,
    WRITE_HEADER_ERROR
};

char* get_write_error_text(enum write_status write_status);

char* get_read_error_text(enum read_status read_status);

#endif
