#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <stdlib.h>

#include "pixel.h"

struct image {
    uint64_t width, height; 
    struct pixel* data;
};

void free_image(struct image* image);

void image_data_allocate(struct image* image);

#endif
