#ifndef BMP_H
#define BMP_H

#include "errors.h"
#include "image.h"

// FILE* open_file_read(const char* file_name);

// FILE* open_file_write(const char* file_name);

// void close_file(FILE* file);

// enum read_status read_from_bmp( FILE* file, struct image* img);

// enum write_status write_to_bmp( FILE* file, const struct image* img);

void check_output_image_header(const char* file_output_name);

void open_and_read_bmp_file(const char* file_input_name, struct image* image);

void open_and_write_bmp_file(const char* file_output_name, struct image* image);

#endif


