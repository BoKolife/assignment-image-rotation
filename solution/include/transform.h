#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "image.h"
#include <stdio.h>  

struct image rotate(const struct image image);

#endif
