#include "bmp.h"

#define BFTYPE 19778
#define PIXELS_PER_METER 2834
#define BITS_IN_PIXEL 24
#define INFO_HEADER_SIZE 40

struct bmp_header    // need for fast bytes reading (60 bytes)
{
        uint16_t bfType;  // file type: BM
        uint32_t bfileSize;  // file size in bytes
        uint32_t bfReserved;   // reserved field
        uint32_t bOffBits;  // bits array offset touching file begin

        uint32_t biSize;  // struct DIBHeader (detailed inform) size
        uint32_t biWidth;  // shirina in pixels (width)
        uint32_t biHeight;  // visota (height)
        uint16_t biPlanes; // always 1 with 256 color
        uint16_t biBitCount;  // bit per pixel, pixel color depth = 3 
        uint32_t biCompression;  // compression type, without compr. = BI_RGB
        uint32_t biSizeImage;  // image size in bytes
        uint32_t biXPelsPerMeter; // horizontal resolution (dev meter) important for device
        uint32_t biYPelsPerMeter; // vertical
        uint32_t biClrUsed; // count of colors in table, if = 0 - max color count, which aloud by biBitCount field
        uint32_t biClrImportant; // main colors count need for image = 0 - all colors
}__attribute__((packed));  // alignment

static FILE *open_file_read(const char* file_name) {
    FILE *f = fopen(file_name, "rb");
    if (!f) {
        fprintf(stderr, "Uncorrect input file\n");
        exit(EXIT_FAILURE);
    }
    else return f;
}

static FILE *open_file_write(const char* file_name) {
    FILE *f = fopen(file_name, "wb");
    if (!f) {
        fprintf(stderr, "Uncorrect output file\n");
        exit(EXIT_FAILURE);
    }
    else return f;
}

static void close_file(FILE* file) {
    if (fclose(file) != 0) {
        fprintf(stderr, "Closing file error\n");
    }
}

static int64_t calc_padding(const struct image* image) {
    return (int64_t) (4 - ((image->width * 3) % 4)) % 4;
}

static void print_header(const struct bmp_header* header) {
    printf("Type: %u\n", header->bfType);
    printf("File size in bytes: %u\n", header->bfileSize);
    printf("Image size in bytes: %u\n", header->biSizeImage);
    printf("Offset: %u\n", header->bOffBits);

    printf("Header size: %u\n", header->biSize);
    printf("Width in pixels: %u\n", header->biWidth);
    printf("Height in pixels: %u\n", header->biHeight);
    printf("Planes: %u\n", header->biPlanes);
    printf("BitCount: %u\n", header->biBitCount);
    printf("Compression: %u\n", header->biCompression);
    printf("Per meter Y: %u\n", header->biYPelsPerMeter);
    printf("Per meter X: %u\n", header->biXPelsPerMeter);
    printf("Colors: %u\n", header->biClrUsed);
    printf("Important: %u\n", header->biClrImportant);
}

/*Funct from_bmp and to_bmp should not open and close files */
static enum read_status read_from_bmp(FILE* file, struct image* img) {
    struct bmp_header pheader = {0};
    struct bmp_header* header = &pheader; // in heap
    const unsigned long f = fread(header, sizeof(struct bmp_header), 1, file);
    if (f > 0){
        img->height = header->biHeight; 
        img->width = header->biWidth;
        image_data_allocate(img);

        const int64_t padding = calc_padding(img);
        if (fseek(file, header->bOffBits, SEEK_SET) != 0){
            return READ_INVALID_FSEEK;
        }
        else {
            for (size_t y = 0; y < img->height; y++){
                for (size_t x = 0; x < img->width; x++){
                    unsigned long z = fread((img->data + (y * img->width + x)), sizeof(struct pixel), 1, file);
                    if (z == 0){
                        return READ_INVALID_DATA;
                    }
                }
                if (fseek(file, padding, SEEK_CUR) != 0){
                    return READ_INVALID_FSEEK;
                }
            }
            // for checking
            print_header(header);

            return READ_OK;
        }
    }
    else return READ_INVALID_FILE;
}

void check_output_image_header(const char* file_output_name){
    //// checking output file 
    //struct image out_image = {0};
    // printf("\nOutput file:\n");
    // FILE* file_out = open_file_read(file_output_name);
    // if (file_out) {
    //     read_from_bmp(file_out, &out_image);
    //     free(out_image.data);
    //     close_file(file_out);
    // }
}

static struct bmp_header create_bmp_header(const struct image* image) {
    struct bmp_header header = {0};
    const int64_t padding = calc_padding(image);

    header.bfType = BFTYPE;                                                                         // file type: BM 
    header.bfileSize = sizeof(struct bmp_header) + (image->height * (image->width * 3 + padding)); // file size in bytes
    header.bOffBits = sizeof(struct bmp_header);                                                   // 54 bits array offset touching file begin

    header.biSize = INFO_HEADER_SIZE;                                      // struct DIBHeader (detailed inform) size (always 40)
    header.biWidth = image->width;                                         // in pixels
    header.biHeight = image->height;
    header.biPlanes = 1;                                                   // always 1 with 256 color
    header.biBitCount = BITS_IN_PIXEL;                                     // bit per pixel, pixel color depth = 3 (8 * 3 = 24)
    header.biCompression = 0;                                              // compression type, without compr. = BI_RGB
    header.biSizeImage = ((3 * image->width + padding) * (image->height)); // image size in bytes
    header.biXPelsPerMeter = PIXELS_PER_METER;                             // horizontal resolution (dev meter) important for device
    header.biYPelsPerMeter = PIXELS_PER_METER;                             // vertical
    header.biClrUsed = 0;                                                  // count of colors in table, if = 0 - max color count, which aloud by biBitCount field
    header.biClrImportant = 0;                                             // main colors count need for image = 0 - all colors

    return header;
}

static enum write_status write_to_bmp(FILE* file, const struct image* img) {
    if (file) {
        printf("\n Writing to file...\n");
        struct bmp_header header = create_bmp_header(img);
        const int64_t padding = calc_padding(img);

        if (fwrite(&header, sizeof(struct bmp_header), 1, file) > 0) {
            for (size_t y = 0; y < img->height; y++) {
                fwrite(img->data + (y * img->width), sizeof(struct pixel), img->width, file);
                if (fseek(file, padding, SEEK_CUR) != 0){
                    return WRITE_INVALID_FSEEK;
                }
            }
            if (fwrite(&padding, 1, 1, file) != 0){
                return WRITE_OK;
            } 
            else return WRITE_ERROR; 
        }
        else return WRITE_HEADER_ERROR;
    }
    else return WRITE_ERROR;
}

void open_and_read_bmp_file(const char* file_input_name, struct image* image){
    FILE* file_in = open_file_read(file_input_name);
    const enum read_status read_status = read_from_bmp(file_in, image);
    if (read_status == READ_OK) {
        close_file(file_in);
    }
    else {
        free_image(image);
        fprintf(stderr, "%s\n", get_read_error_text(read_status));
        exit(EXIT_FAILURE);
    }
}

void open_and_write_bmp_file(const char* file_output_name, struct image* image){
    FILE* out_file = open_file_write(file_output_name);
    const enum write_status write_status = write_to_bmp(out_file, image);
    if (write_status == WRITE_OK){
        close_file(out_file);
        free_image(image);
    }
    else {
        free_image(image);
        fprintf(stderr, "%s\n", get_write_error_text(write_status));
        exit(EXIT_FAILURE);
    }
}


