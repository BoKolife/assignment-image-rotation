#include "image.h" 

void free_image(struct image* image){
    free(image->data);
}

void image_data_allocate(struct image* image){
    image->data = (struct pixel*) malloc((image->height) * (image->width) * sizeof(struct pixel));
}
