#include "transform.h"

static struct pixel* get_address_pixel(struct image const image, size_t y, size_t x){
    return image.data + y * image.width + x; // y - height, x - width
}

/*Transform module knows about struct image, but doesn't know about input formats*/
/* creates copy of image rotated 90 deg */
struct image rotate(const struct image image){
    struct image rotated_image = {0};
    rotated_image.width = image.height;
    rotated_image.height = image.width;
    image_data_allocate(&rotated_image);

    for (size_t y = 0; y < rotated_image.height; y++){ // step by height
        for (size_t x = 0; x < rotated_image.width; x++){ // step by width
            *get_address_pixel(rotated_image, y, x) = *get_address_pixel(image, rotated_image.width - 1 - x, y);
        }
    } 

    return rotated_image;
}
