#include "bmp.h"
#include "transform.h"

#include <stdio.h>

void usage(void) {
    fprintf(stderr, "Usage: ./ name BMP_FILE_NAME_IN BMP_FILE_NAME_OUT\n");
}

int main(int argc, char** argv){ // argc - count of args, argv - array of args
    if (argc == 3) {
        const char* file_input_name = argv[1];
        const char* file_output_name = argv[2];

        struct image p_image = {0};
        struct image* image = &p_image;
        struct image rotated_image = {0};
        
        open_and_read_bmp_file(file_input_name, image);
        rotated_image = rotate(p_image);
        free_image(image);

        open_and_write_bmp_file(file_output_name, &rotated_image);

        // check
        check_output_image_header(file_output_name);
    }
    else {
        fprintf(stderr, "Uncorrect file name arguments\n");
    }

    return 0;
}


