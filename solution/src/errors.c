#include "errors.h"

static char* write_error_enum[] = {
    [WRITE_OK] = "WRITE_OK", 
    [WRITE_ERROR] = "WRITE_ERROR",
    [WRITE_INVALID_FSEEK] = "INVALID_FSEEK",
    [WRITE_HEADER_ERROR] = "WRITE_HEADER_ERROR"
};

static char* read_error_enum[] = {
    [READ_OK] = "READ_OK", 
    [READ_INVALID_DATA] = "READ_INVALID_DATA",
    [READ_INVALID_FSEEK] = "INVALID_FSEEK",
    [READ_INVALID_HEADER] = "READ_INVALID_HEADER",
    [READ_INVALID_FILE] = "READ_INVALID_FILE"
};

char* get_write_error_text(enum write_status write_status){
    return write_error_enum[write_status];
}

char* get_read_error_text(enum read_status read_status){
    return read_error_enum[read_status];
}
